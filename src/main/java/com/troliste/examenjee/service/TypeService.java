package com.troliste.examenjee.service;

import com.troliste.examenjee.models.Type;
import com.troliste.examenjee.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeService {
    @Autowired
    private TypeRepository typeRepository;
    public List<Type> listAll(){
        return typeRepository.findAll();
    }
    public void save(Type product){
        typeRepository.save(product);
    }
    public Type get(Long id){
        return typeRepository.getOne(id);
    }
    public void delete(Long id){
        typeRepository.deleteById(id);
    }
}
