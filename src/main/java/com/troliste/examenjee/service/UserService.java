package com.troliste.examenjee.service;


import com.troliste.examenjee.models.User;
import com.troliste.examenjee.web.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
