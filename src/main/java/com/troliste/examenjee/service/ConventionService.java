package com.troliste.examenjee.service;

import com.troliste.examenjee.models.Convention;
import com.troliste.examenjee.repository.ConventionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConventionService {
    @Autowired
    private ConventionRepository conventionRepository;

    public List<Convention> listAll(){
        return conventionRepository.findAll();
    }
    public void save(Convention convention){
        conventionRepository.save(convention);
    }
    public Convention get(Long id){
        return conventionRepository.getOne(id);
    }
    public void delete(Long id){
        conventionRepository.deleteById(id);
    }
    public List<Convention>  getConventionsByType(long type){
        return conventionRepository.findByType(type);
    }
    public List<Convention>  getConventionsByDateExpiration(String date_expiration){
        return conventionRepository.findByDateExpiration(date_expiration);
    }
    public List<Convention>  getConventionsByTypeAndDateExpiration(long type,String date_expiration){
        return conventionRepository.findByTypeAndDateExpiration(type,date_expiration);
    }
}
