package com.troliste.examenjee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class ExamenjeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamenjeeApplication.class, args);
    }

}
