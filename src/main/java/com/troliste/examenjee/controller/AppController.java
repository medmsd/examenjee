package com.troliste.examenjee.controller;

import com.troliste.examenjee.models.Convention;
import com.troliste.examenjee.models.Participant;
import com.troliste.examenjee.models.SearchForm;
import com.troliste.examenjee.models.User;
import com.troliste.examenjee.service.ConventionService;
import com.troliste.examenjee.service.ParticipantService;
import com.troliste.examenjee.service.TypeService;
import com.troliste.examenjee.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AppController {

    @Autowired
    private UserService userService;
    @Autowired
    private ConventionService conventionService;
    @Autowired
    private ParticipantService participantService;
    @Autowired
    private TypeService typeService;
    public static List<Convention> conventionList = new ArrayList<>();
    private DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    @RequestMapping("/pdf")
    public String pdfView(Model model){
        model.addAttribute("formatter", formatter);
        model.addAttribute("participantService", participantService);
        model.addAttribute("typeService", typeService);
        model.addAttribute("conventionList", conventionList);
        return "pdf";
    }
    @RequestMapping("/printOne/{id}")
    public String printOne(Model model,@PathVariable(name = "id") Long id){
        model.addAttribute("formatter", formatter);
        model.addAttribute("participantService", participantService);
        model.addAttribute("typeService", typeService);
        conventionList=new ArrayList<>();
        conventionList.add(conventionService.get(id));
        model.addAttribute("conventionList", conventionList);
        return "pdf";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }
    @RequestMapping("/")
    public String viewHomePage(Model model, SearchForm searchForm) {
        if(searchForm.getDate_expiration()==null)
            searchForm.setDate_expiration("");
        model.addAttribute("formatter", formatter);
        model.addAttribute("participantService", participantService);
        model.addAttribute("typeService", typeService);
        model.addAttribute("searchForm", searchForm);
        if (searchForm.getType() == 0 && searchForm.getDate_expiration().equals(""))  {
            conventionList = conventionService.listAll();
        } else if (searchForm.getType() != 0 && searchForm.getDate_expiration().equals("") && searchForm.getType()!=0) {
            conventionList = conventionService.getConventionsByType(searchForm.getType());
        } else if (searchForm.getType() == 0) {
            conventionList = conventionService.getConventionsByDateExpiration(searchForm.getDate_expiration());
        }
        else {
            conventionList = conventionService.getConventionsByTypeAndDateExpiration(searchForm.getType(),
                    searchForm.getDate_expiration());
        }
        model.addAttribute("conventionList", conventionList);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = this.userService.findByEmail(auth.getName());
        model.addAttribute("user", user);
        return "index";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveConvention(@ModelAttribute("convention") Convention convention) {
        conventionService.save(convention);
        return "redirect:/";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditConventionForm(@PathVariable(name = "id") Long id) {
        ModelAndView modelAndView = new ModelAndView("edit_convention");
        Convention convention = conventionService.get(id);
        modelAndView.addObject("participantService", participantService);
        modelAndView.addObject("typeService", typeService);
        modelAndView.addObject("convention", convention);
        return modelAndView;
    }

    @RequestMapping("/delete/{id}")
    public String deleteConvention(@PathVariable(name = "id") Long id) {
        conventionService.delete(id);
        return "redirect:/";
    }

    @RequestMapping("/add_convention")
    public String addConvention(Model model) {
        Convention convention = new Convention();
        model.addAttribute("participantService", participantService);
        model.addAttribute("typeService", typeService);
        model.addAttribute("convention", convention);
        return "add_convention";
    }

    @RequestMapping("/add_participant")
    public String addParticipant(Model model) {
        Participant participant = new Participant();
        model.addAttribute("participant", participant);
        return "add_participant";
    }

    @RequestMapping(value = "/saveParticipant", method = RequestMethod.POST)
    public String saveParticipant(@ModelAttribute("participant") Participant participant) {
        participantService.save(participant);
        return "redirect:/";
    }

}
