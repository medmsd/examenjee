package com.troliste.examenjee.models;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "conventions")
public class Convention {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NonNull
    private long type;
    @NonNull
    private long participant1;
    @NonNull
    private long participant2;
    private long participant3;
    private long participant4;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NonNull
    private Date dateEdition;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "date_signature1")
    private Date dateSignature1;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "date_signature2")
    private Date dateSignature2;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "date_signature3")
    private Date dateSignature3;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(name = "date_signature4")
    private Date dateSignature4;
    @Column(name = "date_entre_vigueur")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @NonNull
    private Date dateEntrevigueur;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dateExpiration;
    @NonNull
    private String objet;

    public Convention() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public Date getDateSignature2() {
        return dateSignature2;
    }

    public void setDateSignature2(Date dateSignature2) {
        this.dateSignature2 = dateSignature2;
    }

    public Date getDateSignature3() {
        return dateSignature3;
    }

    public void setDateSignature3(Date dateSignature3) {
        this.dateSignature3 = dateSignature3;
    }

    public Date getDateSignature4() {
        return dateSignature4;
    }

    public void setDateSignature4(Date dateSignature4) {
        this.dateSignature4 = dateSignature4;
    }

    public Date getDateEdition() {
        return dateEdition;
    }

    public void setDateEdition(Date dateEdition) {
        this.dateEdition = dateEdition;
    }

    public Date getDateSignature1() {
        return dateSignature1;
    }

    public void setDateSignature1(Date dateSignature) {
        this.dateSignature1 = dateSignature;
    }

    public Date getDateEntrevigueur() {
        return dateEntrevigueur;
    }

    public void setDateEntrevigueur(Date dateEntrevigueur) {
        this.dateEntrevigueur = dateEntrevigueur;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public long getParticipant1() {
        return participant1;
    }

    public void setParticipant1(long participant1) {
        this.participant1 = participant1;
    }

    public long getParticipant2() {
        return participant2;
    }

    public void setParticipant2(long participant2) {
        this.participant2 = participant2;
    }

    public long getParticipant3() {
        return participant3;
    }

    public void setParticipant3(long participant3) {
        this.participant3 = participant3;
    }

    public long getParticipant4() {
        return participant4;
    }

    public void setParticipant4(long participant4) {
        this.participant4 = participant4;
    }
}
