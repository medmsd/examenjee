package com.troliste.examenjee.repository;

import com.troliste.examenjee.models.Convention;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConventionRepository extends JpaRepository<Convention,Long> {

    @Query(value = "select * from conventions where type=:type ",nativeQuery = true)
    List<Convention> findByType(@Param("type") long type); 
    @Query(value = "select * from conventions  where date_expiration like %:date_expiration% ",nativeQuery = true)
    List<Convention> findByDateExpiration( @Param("date_expiration") String date_expiration);
    @Query(value = "select * from conventions  where type=:type and date_expiration like %:date_expiration% ",nativeQuery = true)
    List<Convention> findByTypeAndDateExpiration(@Param("type") long type,
                                                 @Param("date_expiration") String date_expiration);
}
