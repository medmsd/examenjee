Gestion des Conventions par Mohamed Mseddi
Dans cette projet l'administrateur peut Ajouter/Modifier/Supprimer une convention
et peut aussi Ajouter un participant.
Fonctionnalités :
- Filtre selon Type / Date Expiration
- Imprimer une convention ou resultat de recherche des conventions

Outils :
- Springboot
- Thymeleaf
- Bootstrap
- JpaRepository
- Mysql Database

Authentification :
- email : mameya.mseddi@gmail.com
- password : 1996medmsd

NB : N'oublier pas d'importer la base de données database.sql